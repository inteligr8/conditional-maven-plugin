
# Conditional Maven Plugin

This is a maven plugin that allows for the conditional setting of properties.

## Usage

```xml
<project>
    ...
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>com.inteligr8</groupId>
                <artifactId>conditional-maven-plugin</artifactId>
                <version>...</version>
                <executions>
                    <execution>
                        <id>sample-boolean-test</id>
                        <phase>validate</phase>
                        <goals><goal>boolean</goal></goals>
                        <configuration>
                            <booleanProperty>a.proprety.to.test</booleanProperty>
                            <newProperty>a.new.property.to.set</newProperty>
                            <trueValue>A true value</trueValue>
                            <falseValue>A false value</falseValue>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
    ...
</project>
```

## Goals

| Goal               | Description |
| ------------------ | ----------- |
| `boolean-property` | Set a property based on the value of another boolean property. |
| `file-existence`   | Set a property based on the existence of a file. |

### Goal: `boolean-property`

| Configuration Property | Data Type | Required | Default | Description |
| ---------------------- |:---------:|:--------:| ------- | ----------- |
| `booleanProperty`      | `string`  | Yes      |         | The name of a property.  Only evaluates to `true` if the property value is a boolean or string value of `true` at the time of the execution of this plugin.  If the property does not exist, is of a different data type, `NULL`, or has another value, it evaluates to `false`. |
| `newProperty`          | `string`  | Yes      |         | The name of a new property to create.  An existing property cannot be updated in Maven. |
| `trueValue`            | `string`  |          |         | If the `booleanProperty` configuration evaluates to `true`, use this to set the `newProperty` value.  This may only be a string literal at this time; it is not evaluated itself. |
| `falseValue`           | `string`  |          |         | If the `booleanProperty` configuration evaluates to `false`, use this to set the `newProperty` value.  This may only be a string literal at this time; it is not evaluated itself. |
| `charset`              | `string`  |          | `utf-8` | |
| `skip`                 | `boolean` |          | `false` | |

### Goal: `file-existence`

| Configuration Property | Data Type | Required | Default | Description |
| ---------------------- |:---------:|:--------:| ------- | ----------- |
| `file`                 | `string`  | Yes      |         | The name of a file, relative to the root directory of the Maven project.  Only evaluates to `true` if the file exists and has a non-zero length (see `emptyFileIsTrue` for one exception). |
| `emptyFileIsTrue`      | `boolean` |          | `false` | A zero-length file should evaluate to `true`. |
| `newProperty`          | `string`  | Yes      |         | The name of a new property to create.  An existing property cannot be updated in Maven. |
| `trueValue`            | `string`  |          |         | If the `file`/`emptyFileIsTrue` configurations evaluate to `true`, use this to set the `newProperty` value.  This may only be a string literal at this time; it is not evaluated itself. |
| `falseValue`           | `string`  |          |         | If the `file`/`emptyFileIsTrue` configurations evaluate to `false`, use this to set the `newProperty` value.  This may only be a string literal at this time; it is not evaluated itself. |
| `charset`              | `string`  |          | `utf-8` | |
| `skip`                 | `boolean` |          | `false` | |
