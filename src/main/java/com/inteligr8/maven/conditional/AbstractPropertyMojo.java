/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.conditional;

import javax.annotation.OverridingMethodsMustInvokeSuper;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Requirement;

import com.inteligr8.maven.ProjectPropertyResolver;

public abstract class AbstractPropertyMojo extends AbstractMojo {
	
	@Parameter( defaultValue = "${project}", readonly = true )
	protected MavenProject project;
	
	@Parameter( defaultValue = "${session}", readonly = true )
	protected MavenSession session;
	
	@Requirement
	private ProjectPropertyResolver propResolver;
	
	@Parameter( property = "newProperty", required = true )
	protected String newProperty;
	
	@Parameter( property = "trueValue" )
	protected String trueValue;
	
	@Parameter( property = "falseValue" )
	protected String falseValue;
	
	@Parameter( property = "charset", required = true, defaultValue = "utf-8" )
	protected String charsetName = "utf-8";
	
	@Parameter( property = "skip", required = true, defaultValue = "false" )
	protected boolean skip = false;

    public final void execute() throws MojoExecutionException, MojoFailureException {
    	if (this.skip) {
        	this.getLog().debug("Skipped execution");
    		return;
    	}

    	this.validateParamsPreNormalization();
    	this.normalizeParameters();
    	this.validateParamsPostNormalization();
    	
    	this.go();
    }
    
    protected abstract void go() throws MojoExecutionException, MojoFailureException;

    @OverridingMethodsMustInvokeSuper
    protected void validateParamsPreNormalization() throws MojoFailureException {
    	this.getLog().debug("Validating parameters before their normalization");
    	
    	if (this.newProperty == null)
    		throw new MojoFailureException("The 'newProperty' element is required");
    }

    @OverridingMethodsMustInvokeSuper
    protected void normalizeParameters() throws MojoFailureException {
    	this.getLog().debug("Normalizing parameters");

    	this.newProperty = StringUtils.trimToNull(this.newProperty);
    }

    @OverridingMethodsMustInvokeSuper
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	this.getLog().debug("Validating parameters after their normalization");
    	
    	if (this.newProperty == null)
    		throw new MojoFailureException("The 'newProperty' element is required");
    }
    
    protected final void executeOnCondition(boolean condition) {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("Condition: " + condition);
		
    	if (condition && this.trueValue != null) {
    		if (this.getLog().isDebugEnabled())
    			this.getLog().debug("Settings property: " + this.newProperty + " to '" + this.trueValue + "'");
    		this.project.getProperties().setProperty(this.newProperty, this.trueValue);
    	} else if (!condition && this.falseValue != null) {
    		if (this.getLog().isDebugEnabled())
    			this.getLog().debug("Settings property: " + this.newProperty + " to '" + this.falseValue + "'");
    		this.project.getProperties().setProperty(this.newProperty, this.falseValue);
    	}
    }
    
}
