/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.conditional;

import java.util.Properties;

import javax.annotation.OverridingMethodsMustInvokeSuper;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;

import com.inteligr8.maven.ProjectPropertyResolver;

@Mojo( name = "boolean-property", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class BooleanPropertyMojo extends AbstractPropertyMojo {
	
	@Parameter( property = "booleanProperty", required = true )
	protected String booleanProperty;
	
	@Requirement
	private ProjectPropertyResolver propResolver;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing boolean property condition");

    	boolean value = false;
    	
		Properties props = this.propResolver.resolveScope(this.booleanProperty);
		if (props == null) {
			// assume false if it doesn't exist
		} else {
			String strValue = props.getProperty(this.booleanProperty);
			value = Boolean.parseBoolean(strValue);
		}
		
    	this.executeOnCondition(value);
    }
    
    @Override
    @OverridingMethodsMustInvokeSuper
    protected void validateParamsPreNormalization() throws MojoFailureException {
    	super.validateParamsPreNormalization();
    	
    	if (this.booleanProperty == null)
    		throw new MojoFailureException("The 'booleanProperty' element is required");
    }

    @Override
    @OverridingMethodsMustInvokeSuper
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	this.booleanProperty = StringUtils.trimToNull(this.booleanProperty);
    }
    
    @Override
    @OverridingMethodsMustInvokeSuper
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	super.validateParamsPostNormalization();
    	
    	if (this.booleanProperty == null)
    		throw new MojoFailureException("The 'booleanProperty' element is required");
    }
    
}
