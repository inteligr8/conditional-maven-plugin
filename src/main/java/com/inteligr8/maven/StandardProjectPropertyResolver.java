/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven;

import java.util.Properties;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Profile;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;

@Component(role = ProjectPropertyResolver.class)
public class StandardProjectPropertyResolver implements ProjectPropertyResolver {
	
	@Requirement
	private MavenSession session;
	
	@Requirement
	private MavenProject project;
	
	@Override
	public String resolve(String propertyName) {
    	Properties props = this.findPropertiesObject(propertyName);
    	return props == null ? null : props.getProperty(propertyName);
    }
	
	@Override
	public String resolve(String propertyName, String defaultValue) {
    	Properties props = this.findPropertiesObject(propertyName);
    	return props == null ? null : props.getProperty(propertyName, defaultValue);
    }
	
	@Override
	public Properties resolveScope(String propertyName) {
		return this.findPropertiesObject(propertyName);
	}
    
    private Properties findPropertiesObject(String key) {
    	// search the user/cli properties first
    	Properties props = this.session.getUserProperties();
    	if (props.containsKey(key))
    		return props;
    	
    	// search the profiles next; in order (FIXME maybe we should go backwards?)
    	for (Profile profile : this.project.getActiveProfiles()) {
    		props = profile.getProperties();
    		if (props.containsKey(key))
    			return props;
    	}
    	
    	// now look at the project props
    	props = this.project.getProperties();
		if (props.containsKey(key))
			return props;
		
		// now recursively look up the parent project props
		MavenProject ancestor = this.project.getParent();
		while (ancestor != null) {
			props = ancestor.getProperties();
			if (props.containsKey(key))
				return props;
			ancestor = ancestor.getParent();
		}

    	// search the system properties last (FIXME is this right?)
    	props = this.session.getSystemProperties();
    	if (props.containsKey(key))
    		return props;
		
		return null;
    }

}
